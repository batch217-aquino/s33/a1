//console.log("Hello World!");

//[SECTION] - JavaScript Synchronous vs Asynchronous
//JS -->  Synchronous - it can execute one statement or one line of code at a time.

console.log("Hello World!");
//conosle.log("Hello Again!");
console.log("Goodbye");

console.log("Hello World!");
//for(let i = 0; i <= 1500; i++){
//	console.log(i);
//}
console.log("Hello Again!");

//[SECTION] - Geeting all post
//fetch() - fetch request
//The fetch API allows programmers to asynchronously request for a resource (data).

/*

	Syntax -- //fecth()
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

*/

fetch("https://jsonplaceholder.typicode.com/posts")
//We use "json" methon from the response object to convert data into JSON format
.then((response) => response.json())
.then((json) => console.log(json));

//The "async" and "await" keywords is another apprach that can be used to achieve asynchronous
//Used in functions to indicate which portions of the code should be awaited for.
//Creates an asynchronous function

async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	//result returned "response" -- expecting a promise
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

//[SECTION] Getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Creating a post

/*

SYNTAX:

fetch("URL", options)
.then((response) => {})
.then((response) => {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Updating a post
//PUT Method - is used to update a specific data

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Patch method
//Update a specific post, specific property in a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

//[SECTION] Filtering Post

/*
	SYNTAX
	Individual Parameter
	// 'url?parameterName=value'
	Multiple Parameter
	//url?paramA=valueA&paramB=valueB
*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=2")
.then((response) => response.json())
.then((json) => console.log(json));

//[SECTION] Retrieving comments on a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));